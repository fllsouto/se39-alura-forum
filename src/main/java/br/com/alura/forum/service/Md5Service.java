package br.com.alura.forum.service;

import java.security.MessageDigest;

import javax.xml.bind.DatatypeConverter;

import org.springframework.stereotype.Service;

@Service
public class Md5Service {

	public String hashPassword(String raw) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(raw.getBytes());
			byte[] digest = md.digest();
			return DatatypeConverter.printHexBinary(digest).toLowerCase();
		} catch (Exception e) {
			throw new RuntimeException("Erro ao gerar hash MD5 da senha!");
		}
	}

}
